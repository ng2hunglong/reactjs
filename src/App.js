import React, {Component} from 'react'
import Products from './vschoolProducts'
class App extends Component {
  productTable;
  render() {
    this.productTable = Products.map((product) => {
      return <React.StrictMode key={product.id}>
        <tr>
          <td>{product.id}</td>
          <td>{product.name}</td>
          <td>{product.price}</td>
          <td>{product.description}</td>
        </tr>
      </React.StrictMode>
    });
    return <React.StrictMode>
    <table>
      <thead>
        <tr>
          <th>id</th>
          <th>name</th>
          <th>price</th>
          <th>description</th>
        </tr>
      </thead>
      <tbody>
        {this.productTable}
      </tbody>
    </table>
    </React.StrictMode>;
  }
}

export default App