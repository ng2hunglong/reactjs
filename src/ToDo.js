import React, { Component } from 'react'
import ToDoList from './ToDoList';

class ToDo extends Component {

  constructor() {
    super();
    this.state = {
      toDoList: ToDoList,
    };
    this.completedStyle = {
      color: "#cdcdcd",
      fontStyle: 'italic',
      textDecoration: 'line-through'
    };
    this.onClickHandle = this.onClickHandle.bind(this);
  }

  onClickHandle(id) {
    this.setState(prevState => {
      const updatedState = prevState.toDoList.map(item => {
        if (item.id === id) {
          item.isCompleted = !item.isCompleted;
        }
        return item;
      })
      return {
        toDoList: updatedState,
      };
    })
  }

  render() {
    return this.state.toDoList.map(item => {
      return <React.StrictMode key={item.id}>
        <div>
          <input type="checkbox" checked={item.isCompleted} onChange={() => {this.onClickHandle(item.id)}}></input>
          <span style={item.isCompleted ? this.completedStyle : null}>{item.description}</span>
        </div>
      </React.StrictMode>
    })
  }
}

export default ToDo