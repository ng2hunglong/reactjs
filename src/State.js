import React, { Component } from 'react'

class State extends Component {
  constructor() {
    super();
    this.state = {
      count: 0,
      numb: 12,
    }
    this.add = this.add.bind(this);
    this.subtract = this.subtract.bind(this);
  }
  add() {
    this.setState((state) => {
      return {
        count: state.count + 1,
        numb: state.numb + 1,
      }
    })
  }
  subtract() {
    this.setState((prevState) => {
      return {
        count: prevState.count - 1,
        numb: prevState.numb - 1,
      }
    })
  }
  render() {
    return <React.StrictMode>
      <p>{this.state.numb}</p>
      <h1>{this.state.count}</h1>
      <button onClick={this.add}>Increase Count</button>
      <button onClick={this.subtract}>Decrease Count</button>
    </React.StrictMode>
  }
}

export default State;