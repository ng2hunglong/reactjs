import React, { Component } from 'react'

class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      isLoading: true,
    }
  }
  componentDidMount() {
    this.InterVal = setInterval(() => {this.tick()}, 1000);
    setTimeout(() => {
      this.setState({
        isLoading: false,
      })
    }, 3000);
  }
  componentWillUnmount() {
    console.log('clearInterval');
    clearInterval(this.InterVal);
  }
  tick() {
    this.setState({
      date: new Date(),
    })
  }
  render() {
    return <React.StrictMode>
      <span>Time: {this.state.date.toLocaleTimeString()}</span>
      <div>Is loading: {this.state.isLoading ? "True" : "False"}</div>
    </React.StrictMode>
  }
}

export default Clock