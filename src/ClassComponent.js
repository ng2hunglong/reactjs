import React, { Component } from "react"
import vschoolProduct from './vschoolProducts'

class ClassComponent extends Component {
  constructor() {
    super();
    this.state = {
      loginStatus: true
    }
    this.productArr = vschoolProduct.map((product) => {
      return <React.StrictMode key={product.id}>
        <tr>
          <td>{product.id}</td>
          <td>{product.name}</td>
          <td>{product.price}</td>
          <td>{product.description}</td>
        </tr>
      </React.StrictMode>
    })
  }

  render() {
    return <React.StrictMode>
      <h1> You're currently log {this.state.loginStatus ? "in" : "out"}</h1>
      <table>
        <tr>
          <th>id</th>
          <th>name</th>
          <th>price</th>
          <th>description</th>
        </tr>
        {this.productArr}
      </table>
    </React.StrictMode>
  }
}

export default ClassComponent;