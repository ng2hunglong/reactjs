let ToDoList = [
  {
    id: 1,
    description: "Shopping",
    isCompleted: true,
  },
  {
    id: 2,
    description: "Cooking",
    isCompleted: false,
  },
  {
    id: 3,
    description: "Cleaning",
    isCompleted: true,
  },
  {
    id: 4,
    description: "Dumping Trash",
    isCompleted: false,
  },
  {
    id: 5,
    description: "Washing clothes",
    isCompleted: true,
  },
];
export default ToDoList