import React, {Component} from 'react'

class FormHandling extends Component {
  constructor() {
    super();
    this.state = {
      firstName: '',
      lastName: '',
      quote: '',
      isFriendly: true,
    }
    // this.handleInput = this.handleInput.bind(this);
  }

  handleInput(event) {
    console.log(this)
    const {name, value, checked} = event.target;
    if (event.target.type === 'checkbox') {
      this.setState({
        isFriendly: checked,
      })
    } else {
      this.setState({
        [name]: value,
      })
    }
  }

  render() {
    return <React.StrictMode>
      <div>
        <input 
          type="text" 
          name="firstName" 
          value={this.state.firstName}
          onChange={this.handleInput}
        />
      </div>
      <div>
        <input 
          type="text" 
          name="lastName" 
          value={this.state.lastName}
          onChange={this.handleInput}
        />
      </div>
      <div>
        <textarea 
          name="quote" 
          value={this.state.quote}
          onChange={this.handleInput}
        />
      </div>
      <div>
        <input 
          type="checkbox" 
          checked={this.state.isFriendly}
          onChange={this.handleInput}
        />
        <span>is Friendly?</span>
      </div>
      <br />
      <h1>Hello {this.state.firstName} {this.state.lastName}</h1>
      <p>I {!this.state.isFriendly ? "don't" : null} want to be your friend because your quote is:</p>
      <p>{this.state.quote}</p>
    </React.StrictMode>    
  }
}

export default FormHandling