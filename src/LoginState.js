import React, { Component } from 'react'

class LoginState extends Component {
  constructor() {
    super();
    this.state = {
      isLoggedIn: false,
    }
    this.Login = this.Login.bind(this);
    this.Logout = this.Logout.bind(this);
  }
  Login() {
    this.setState({
      isLoggedIn: true,
    })
  }
  Logout() {
    this.setState({
      isLoggedIn: false,
    })
  }
  render() {
    return <React.StrictMode>
      <h1>You are currently log {this.state.isLoggedIn ? "in" : "out"}</h1>
      <button onClick={this.Login}>Login</button>
      <button onClick={this.Logout}>Logout</button>
    </React.StrictMode>
  }
}

export default LoginState