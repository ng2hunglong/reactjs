import React, { Component } from 'react'
import Logo from './icon.png';

class EventHandler extends Component {
  constructor() {
    super();
  }
  handleOnMouseOver() {
    console.log('mouse over')
  }
  render() {
    return <React.StrictMode>
      <img alt="avatar" src={Logo} onMouseOver={this.handleOnMouseOver} />
    </React.StrictMode>
  }
}

export default EventHandler