import React, { Component } from 'react'

class TestFetch extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      toDoList: [],
    }
    this.fetchJson();
  }

  fetchJson() {
    fetch("https://bitbucket.org/ng2hunglong/testapi/raw/9ae6d6f2f670dc2cb86c899ff5052862642bbf4e/todo.json")
    .then(response => {
      return response.json();
    })
    .then(data => {
      this.setState({
        toDoList: JSON.parse(data),
        isLoading: false,
      })
      console.log(this.state.toDoList);
    })
  }

  render() {
    return <React.StrictMode>
      <h1>{this.state.isLoading ? "Loading" : "Loaded"}</h1>
      <ul>
          {this.state.toDoList.map(item => {
            return <React.StrictMode key={item.id}>
              <li>
                <input type="checkbox" checked={item.isCompleted} />
                <span>{item.description}</span>
              </li>
            </React.StrictMode>
          })}
        </ul>
    </React.StrictMode>
  }
}

export default TestFetch