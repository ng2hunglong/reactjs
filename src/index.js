import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
// import ClassComponent from './ClassComponent'
// import EventHandler from './EventHandler'
// import App from './App'
// import State from './State'
// import ToDo from './ToDo'
// import Clock from './Clock'
// import LoginSate from './LoginState'
// import TestFetch from './TestFetch'
// import FormHandling from './FormHandling'

// ReactDOM.render(
//   <LoginSate />,
//   document.getElementById('root')
// )

// ReactDOM.render(
// app.render(),
// <App />,
// <EventHandler />,
// <ClassComponent />,
// <State />,
// <ToDo />,
// <Clock />,
// <TestFetch />, 
// <FormHandling />,
//   document.getElementById('root')
// )



class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
    this.log = this.log.bind(this);
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  log() {
    console.log(this)
  }
  
  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
        <button onClick={this.log}>log</button>
      </div>
    );
  }
}

ReactDOM.render(
  <Clock />,
  document.getElementById('root')
);